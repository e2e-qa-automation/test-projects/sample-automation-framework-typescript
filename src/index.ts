import { apiRequests } from './api-requests'
import { apiUtils } from './api-utils'
import { customAssert } from './custom-assert'
import { environment } from './environment'
import { lib } from './lib'
import { webPages } from './web-pages'
import { webUtils } from './web-utils'

export const framework = {
  apiRequests,
  apiUtils,
  customAssert,
  environment,
  lib,
  webPages,
  webUtils,
}
