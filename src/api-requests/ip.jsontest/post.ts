import { AxiosResponse } from 'axios'

import { apiUtils } from '../../api-utils'

type PostResponse = {
  ip: string
}

export const post = (): Promise<AxiosResponse<PostResponse>> => apiUtils.axiosPromise(
  'post',
  'http://ip.jsontest.com/',
  {},
  {
    headers: {
      'Content-Type': 'application/json',
    },
  },
)
