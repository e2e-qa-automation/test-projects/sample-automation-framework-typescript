import { githubApi } from './github'
import { ipJsontestApi } from './ip.jsontest'

export const apiRequests = {
  githubApi,
  ipJsontestApi,
}
