import { createPathDirectories } from './createPathDirectories'
import { rootPath } from './rootPath'
import { writeInFile } from './writeInFile'

export const fsUtils = {
  createPathDirectories,
  rootPath,
  writeInFile,
}
