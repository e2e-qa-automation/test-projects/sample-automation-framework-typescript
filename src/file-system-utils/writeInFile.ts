import fs from 'fs'
import path from 'path'

import { createPathDirectories } from './createPathDirectories'

export const writeInFile = (dirPath: string, fileName: string, data: string | Buffer, options?: fs.WriteFileOptions) => {
  createPathDirectories(dirPath)
  fs.writeFileSync(path.join(dirPath, fileName), data, options)
}
