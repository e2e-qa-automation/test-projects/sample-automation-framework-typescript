import { Browser, Builder } from 'selenium-webdriver'
require('chromedriver')

import { environment } from '../environment'

export const buildWebDriver = (browserName?: string) => {
  if (environment.seleniumHubHost) {
    return new Builder()
      .forBrowser(browserName || Browser.CHROME)
      .usingServer(environment.seleniumHubHost)
      .build()
  } else {
    return new Builder()
      .forBrowser(browserName || Browser.CHROME)
      .build()
  }
}
