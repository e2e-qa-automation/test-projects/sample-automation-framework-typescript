import path from 'path'

import { WebDriver } from 'selenium-webdriver'

import { fsUtils } from '../../src/file-system-utils'
import { webUtils } from '../../src/web-utils'

describe('@sample @web Run sample web test with selenium', function() {
  let webDriver: WebDriver

  it('init browser', async function() {
    webDriver = await webUtils.buildWebDriver()
  })
  it('navigate to url', async function() {
    await webDriver.get('https://example.com')
  })
  it('take page screenshot', async function() {
    const encodedString = await webDriver.takeScreenshot()
    await fsUtils.writeInFile(
      path.join(
        fsUtils.rootPath,
        'test-resources/screenshots/sample-selenium',
      ),
      'test-screenshot.png',
      encodedString,
      'base64',
    )
  })
  it('close browser', async function() {
    await webDriver.close()
    await webDriver.quit()
  })
})
